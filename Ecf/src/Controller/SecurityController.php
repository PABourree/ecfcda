<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Ticket;
use App\Entity\Comment;
use App\Entity\Category;
use App\Form\CommentType;
use App\Form\RegistrationType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Persistence\ObjectMananger;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class SecurityController extends AbstractController
{
    /**
     * @Route("/inscription", name="security_registration")
     */
    public function registration(Request $request, EntityManagerInterface  $manager, UserPasswordEncoderInterface $encoder) {
        $user = new User();

        $form = $this->createForm(RegistrationType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $hash = $encoder->encodePassword($user, $user->getPassword());

            $user->setPassword($hash);
            $user->setLevel(1);

            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('security_login');
        }

        return $this->render('security/registration.html.twig',[
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("/modifUser", name="security_modifUser")
     */
    public function modifUser() {

        $repo = $this->getDoctrine()->getRepository(User::class);

        $users = $repo->findAll();

        return $this->render('security/modifUser.html.twig', [
            'controller_name' => 'SecurityController',
            'users' => $users
        ]);
    }
    /**
     * @Route("/editUser/{id}", name="security_editUser")
     */
    public function editUser($id, Request $request, EntityManagerInterface  $manager, UserPasswordEncoderInterface $encoder) {
        
        $repo = $this->getDoctrine()->getRepository(User::class);

        $user = $repo->find($id);

        dump($user);

        $formBuilder = $this->createFormBuilder($user)
                     ->add('email')
                     ->add('username')
                     ->add('password', PasswordType::class)
                     ->add('confirm_password', PasswordType::class)
                     ->add('level', ChoiceType::class, [
                         'choices' => [
                             'Utilisateur normal' => 1,
                             'Utilisateur Support technique' => 2,
                             'Administrateur' => 3,
                         ],
                     ]);
        $form = $formBuilder->getForm();
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $hash = $encoder->encodePassword($user, $user->getPassword());

            $user->setPassword($hash);

            $manager->persist($user);
            $manager->flush();
            return $this->render('security/editUser.html.twig',[
                'form' => $form->createView()
            ]);
        }
        
        return $this->render('security/editUser.html.twig',[
            'form' => $form->createView()
        ]);
    }
    

    /**
     * @Route("/createAdmin", name="security_createAdmin")
     */
    public function createadmin(Request $request, EntityManagerInterface  $manager, UserPasswordEncoderInterface $encoder) {
        $user = new User();

        $formBuilder = $this->createFormBuilder($user)
                     ->add('email')
                     ->add('usernamme')
                     ->add('password', PasswordType::class)
                     ->add('confirm_password', PasswordType::class)
                     ->add('level', ChoiceType::class, [
                         'choices' => [
                             'Utilisateur normal' => 1,
                             'Utilisateur Support technique' => 2,
                             'Administrateur' => 3,
                         ],
                     ]);
        $form = $formBuilder->getForm();
        $form->handleRequest($request);
        dump($form);

        if($form->isSubmitted() && $form->isValid()) {
            $hash = $encoder->encodePassword($user, $user->getPassword());

            $user->setPassword($hash);

            $manager->persist($user);
            $manager->flush();

            dump($form);
            //return $this->redirectToRoute('security_login');
            return $this->render('security/createAdmin.html.twig',[
                'form' => $form->createView()
            ]);
        }

        return $this->render('security/createAdmin.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/connexion", name="security_login")
     */
    public function login(){
        return $this->render('security/login.html.twig');
    }

    /**
     * @Route("/deconnexion", name="security_logout")
     */
    public function logout() {
        return $this->redirectToRoute('home');
    }
}
