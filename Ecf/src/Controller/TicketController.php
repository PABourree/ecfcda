<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ObjectMananger;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Ticket;
use App\Entity\Category;
use App\Entity\Comment;
use App\Form\CommentType;



class TicketController extends AbstractController
{
    /**
     * @Route("/ticket", name="ticket")
     */
    public function index(): Response
    {
        $repo = $this->getDoctrine()->getRepository(Ticket::class);

        $tickets = $repo->findAll();
        
        return $this->render('ticket/index.html.twig', [
            'controller_name' => 'TicketController',
            'tickets' => $tickets
        ]);
    }

    /**
     * @Route("/", name="home")
     */
    public function home() {
        return $this->render('ticket/home.html.twig');
    }

    /**
     * @Route("/ticket/{id}/remove", name="ticket_remove")
     */
    public function etat($id, Request $request, EntityManagerInterface  $manager) {

        $repo = $this->getDoctrine()->getRepository(Ticket::class);

        $ticket = $repo->find($id);

        $ticket->setEtat("Ferme");

        $manager->persist($ticket);
        $manager->flush();

        return $this->redirectToRoute('ticket');
    }
    /**
     * @Route("/ticket/new", name="ticket_create")
     * @Route("/ticket/{id}/edit", name="ticket_edit")
     */
    public function form(Ticket $ticket = null, Request $request, EntityManagerInterface  $manager) {

        $user = $this->get('security.token_storage')->getToken()->getUser();
        
        if(!$ticket){
        $ticket = new Ticket();
        }

        $form = $this->createFormBuilder($ticket)
                     ->add('title')
                     ->add('category', EntityType::class, [
                         'class' => Category::class,
                         'choice_label' => 'title'
                     ])
                     ->add('content')
                     ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $ticket->setAuthor($user);

            if(!$ticket->getId()){
                $ticket->setCreatedAt(new \DateTime());
                $ticket->setEtat("Ouvert");
            }
            
            $manager->persist($ticket);
            $manager->flush();

            return $this->redirectToRoute('ticket_show', [
                'id' => $ticket->getId()
                ]);
        }

        return $this->render('ticket/create.html.twig', [
            'formTicket' => $form->createView(),
            'editMode' => $ticket->getId() !== null
        ]);
    }           

    /**
     * @Route("/ticket/own", name="ticket_own")
     */
    public function mesTickets() {

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $repo = $this->getDoctrine()->getRepository(Ticket::class);

        $tickets = $repo->findAll();

        dump($user);
        dump($tickets);

        return $this->render('ticket/own.html.twig', [
            'controller_name' => 'TicketController',
            'tickets' => $tickets,
            'user' => $user
        ]);
    }
    /**
     * @Route("/categorie", name="categorie")
     */
    public function Categorie(){
        $repo = $this->getDoctrine()->getRepository(Category::class);

        $categories = $repo->findAll();

        return $this->render('categorie.html.twig', [
            'controller_name' => 'ticketController',
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/categorieModif/{id}", name="categorie_modif")
     */
    public function modifCategorie($id, Request $request, EntityManagerInterface  $manager){
        $repo = $this->getDoctrine()->getRepository(Category::class);

        $category = $repo->find($id);

        $formBuilder = $this->createFormBuilder($category)
                            ->add('title')
                            ->add('description');
        $form = $formBuilder->getForm();
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            
            $manager->persist($category);
            $manager->flush();
            
        return $this->render('ticket/home.html.twig');
    }
        return $this->render('categorieModif.html.twig',[
        'form' => $form->createView()
    ]);
    }

    /**
     * @Route("/categoryAdd", name="categorie_add")
     */
    public function categoryAdd(Request $request, EntityManagerInterface  $manager){
        $category = new Category();

        $form = $this->createFormBuilder($category)
                     ->add('title')
                     ->add('description')
                     ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

        $manager->persist($category);
        $manager->flush();

        return $this->render('ticket/home.html.twig');

        }

        return $this->render('newcategory.html.twig',[
        'form' => $form->createView()
    ]);
    }

    /**
     * @Route("/categoryRemove/{id}", name="categorie_remove")
     */
    public function categoryRemove($id, Request $request, EntityManagerInterface  $manager){

            $repo = $this->getDoctrine()->getRepository(Ticket::class);

            $tickets = $repo->findBy(array('category' => $id));

            for ($i = 0; $i <= (count($tickets)-1); $i++){
                
                $ticket = $tickets[$i];
                $ticket->setCategory( null );

                $manager->persist($ticket);
                $manager->flush();

            }

            dump($tickets);

            $repo = $this->getDoctrine()->getRepository(Category::class);
    
            $category = $repo->find($id);
    
            $manager->remove($category);
            $manager->flush();
    
            return $this->render('ticket/home.html.twig');
    }
    /**
     * @Route("/ticket/{id}", name="ticket_show")
     */
    public function show($id, Ticket $ticket, Request $request, EntityManagerInterface  $manager){
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $comment->setCreatedAt(new \DateTime())
                    ->setTicket($ticket);


            $manager->persist($comment);
            $manager->flush();

            return $this->redirectToRoute('ticket_show', [
                'id' => $ticket->getId()
            ]);
        }

        $repo = $this->getDoctrine()->getRepository(Ticket::class);

        $ticket = $repo->find($id);

        return $this->render('ticket/show.html.twig', [
            'ticket' => $ticket,
            'commentForm' => $form->createView()
        ]);
    }
}
