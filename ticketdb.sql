-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 29 avr. 2021 à 03:13
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `ticketdb`
--

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `category`
--

INSERT INTO `category` (`id`, `title`, `description`) VALUES
(2, 'casse', 'c\'est cassé'),
(3, 'cella la ui', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_id` int(11) DEFAULT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9474526C700047D2` (`ticket_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `comment`
--

INSERT INTO `comment` (`id`, `ticket_id`, `author`, `content`, `created_at`) VALUES
(1, 1, 'pierre', 'ca marche pas c\'est vrai', '2021-04-15 13:21:58'),
(2, 1, 'pierre 1', 'ca marche encore pas', '2021-04-28 19:16:42');

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20210428090735', '2021-04-28 09:08:34', 43),
('DoctrineMigrations\\Version20210428110620', '2021-04-28 11:07:10', 194),
('DoctrineMigrations\\Version20210428111057', '2021-04-28 11:11:07', 83),
('DoctrineMigrations\\Version20210428113313', '2021-04-28 11:33:22', 39),
('DoctrineMigrations\\Version20210428113412', '2021-04-28 11:34:18', 48),
('DoctrineMigrations\\Version20210428123041', '2021-04-28 12:30:48', 96),
('DoctrineMigrations\\Version20210428193644', '2021-04-28 19:45:29', 150),
('DoctrineMigrations\\Version20210428200353', '2021-04-28 20:03:59', 89),
('DoctrineMigrations\\Version20210428210849', '2021-04-28 21:08:54', 118);

-- --------------------------------------------------------

--
-- Structure de la table `ticket`
--

DROP TABLE IF EXISTS `ticket`;
CREATE TABLE IF NOT EXISTS `ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` datetime NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `etat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_97A0ADA312469DE2` (`category_id`),
  KEY `IDX_97A0ADA3F675F31B` (`author_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ticket`
--

INSERT INTO `ticket` (`id`, `title`, `content`, `created_at`, `category_id`, `author_id`, `etat`) VALUES
(1, 'ticket test 1', 'la voiture est cassée', '2021-04-21 11:18:48', NULL, NULL, 'Ferme'),
(2, 'ticket 2 test', 'La voiture est rayée', '2021-04-21 11:18:48', 2, NULL, 'Ferme'),
(3, 'ticket test 3', 'Le vélo est cassé', '2021-04-28 09:59:32', NULL, NULL, 'Ferme'),
(4, 'ticket test 4', 'La porte est cassée', '2021-04-28 10:12:06', NULL, NULL, 'Ferme'),
(5, 'ticket test 5', 'le ballon crevé', '2021-04-28 10:56:32', NULL, NULL, 'Ferme'),
(6, 'testticket6', 'auteurtest', '2021-04-28 19:47:18', NULL, 3, ''),
(7, 'testbb', 'bb', '2021-04-28 20:39:27', 2, 4, ''),
(8, 'testetat', 'l\'etat du ticket', '2021-04-28 21:13:11', NULL, 4, 'Ouvert'),
(9, 'test ccp', 'ccp', '2021-04-28 21:43:48', 2, 5, 'Ouvert'),
(10, 'zz', 'zz', '2021-04-28 22:33:48', NULL, NULL, 'Ouvert'),
(11, 'aaa', 'aaa', '2021-04-28 22:35:30', 2, 6, 'Ouvert');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usernamme` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `usernamme`, `password`, `username`, `level`) VALUES
(1, 'ZERE@g.g', 'pab', '$2y$13$kjBYRL8oYaG94Unl0uukqe15y0t8snwKCbn6/jSju9M31vBX8jZj2', NULL, 1),
(2, 'pierre.b221@gmail.com', NULL, '$2y$13$5clUynHye22/swaJFwMmdexBA.Ro82zUyVNsKMPSaQ58GRJq39SYG', 'pa', 1),
(3, 'a@a.a', NULL, '$2y$13$5M/rR4G57MDTUF/xKU/tCORC2quvYwTyESH6D0yGXgnYhYo05yWPK', 'aa', 1),
(4, 'b@b.b', NULL, '$2y$13$ETvrKyxnvDJF7OrRRg645eT5JAwOQFiHOmXFovmmtfzr8CfCnXW4K', 'bb', 2),
(5, 'c@c.c', NULL, '$2y$13$/Uq7j1givSpuLy75un2AzeWOCw1TqflMjLj1f9xIx9G4XV7AZhaSW', 'cc', 3),
(6, 'z@z.z', NULL, '$2y$13$sACIgiZzI4co0zbrdjdwfOm/zQ6vF2ByBc0gFK/kmunvoegPmHveK', 'zz', 1),
(7, 'y@y.y', 'yy', '$2y$13$nnXl97vsC6Pw0y8ZyBtdM.v1GexkUZ.oE1LfEwwv6sP4DOZTEDZk2', NULL, 1),
(8, 'w@w.w', 'ww', '$2y$13$FrDlw4ix9Ns72Reg/ykDG.ICqGJg9viS.d7phO2eZ6yUM8ZHK6Ac.', NULL, 1),
(9, 'w@w.wa', 'wwa', '$2y$13$W4U9BCQ1rhaOhY7alZol4OA2DGQKUnku2taqYKKimqUw1tC4KZ1P.', NULL, 1),
(10, 'w@w.wz', 'wwz', '$2y$13$6OApYTMRBXDTOuhkURGXeeSy1THCTTaAebqVTFB66Vzr/KI4YGgpK', NULL, 1),
(11, 'w@w.wza', 'wwz', '$2y$13$NBDYCy.HtxHS92dcTRQE/.1l41kVvXCAqUY3PFeA2gWbHg7UBkSHq', NULL, 1),
(12, 'w@w.wzaa', 'wwz', '$2y$13$YZJrufkf1LO/xyQnN6HZ0.nVv57VCUSsNBYk5ApkyUBki9JYvSBvS', NULL, 1),
(13, 'g@g.g', 'gg', '$2y$13$HzF.Js6vxRdUYfRGbaOMU.8dtIjJ9ETk6S2Po9XIZpZdj161.yAAu', NULL, 3);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `FK_9474526C700047D2` FOREIGN KEY (`ticket_id`) REFERENCES `ticket` (`id`);

--
-- Contraintes pour la table `ticket`
--
ALTER TABLE `ticket`
  ADD CONSTRAINT `FK_97A0ADA312469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `FK_97A0ADA3F675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
